//Imports nativos localizados em node_modules
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

//Import nas paginas do app
import { MyApp } from './app.component';
import { SobrePage } from '../pages/sobre/sobre';
import { AvaliarPage } from '../pages/avaliar/avaliar';
import { ContatoPage } from '../pages/contato/contato';
import { HomePage } from '../pages/home/home';
import { SegmentoPage } from '../pages/segmento/segmento';
import { EmpresaPage } from '../pages/empresa/empresa';
import { TabinfoPage } from '../pages/tabinfo/tabinfo';
import { TabpromoPage } from '../pages/tabpromo/tabpromo';
import { TablocalizacaoPage } from '../pages/tablocalizacao/tablocalizacao';

//Import do provider
import {AppData} from '../providers/app-data';

//tabs
import { SuperTabsModule } from 'ionic2-super-tabs';

//Check network connection
import { Network } from '@ionic-native/network';

@NgModule({
  declarations: [
    MyApp,
    SobrePage,
    AvaliarPage,
    ContatoPage,
    HomePage,
    SegmentoPage,
    EmpresaPage,
    TabinfoPage,
    TabpromoPage,
    TablocalizacaoPage
  ],
  imports: [
    // To see more configs http://ionicframework.com/docs/v2/api/config/Config/
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      tabsPlacement: 'top'
    }, {}),
    SuperTabsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SobrePage,
    AvaliarPage,
    ContatoPage,
    HomePage,
    SegmentoPage,
    EmpresaPage,
    TabinfoPage,
    TabpromoPage,
    TablocalizacaoPage
  ],
  providers: [
    AppData,
    Network
    ,{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
