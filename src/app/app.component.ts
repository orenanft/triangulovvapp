//Imports nativos localizados em node_modules
import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, AlertController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

//Paginas sidemenu e inicial
import { SobrePage } from '../pages/sobre/sobre';
import { AvaliarPage } from '../pages/avaliar/avaliar';
import { ContatoPage } from '../pages/contato/contato';
import { HomePage } from '../pages/home/home';

//Check network connection
import { Network } from '@ionic-native/network';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  //Pagina Raiz do App
  rootPage: any = HomePage;

  pages: Array<{title: string, icon: string, component: any}>;

  constructor(public platform: Platform, private network: Network,private alertCtrl: AlertController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    //Paginas no sidemenu
    this.pages = [
      { title: 'O Triângulo de Vila Velha', icon:'flag' ,component: SobrePage },
      { title: 'Avalie o App', icon:'star', component: AvaliarPage },
      { title: 'Contato', icon:'contacts', component: ContatoPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      // watch network for a connection
        if(this.network.type === 'none'){
          this.presentAlert();
          Splashscreen.hide();
        }else{
          StatusBar.show();
          StatusBar.styleDefault();
          StatusBar.overlaysWebView(true);
          StatusBar.backgroundColorByHexString('#202779');
          StatusBar.styleBlackOpaque();
          Splashscreen.hide();
        }

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    //this.nav.setRoot(page.component);
    this.nav.push(page.component, {title:page.title});
  }

    //Show toast on empresa == null
  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Atenção',
      subTitle: 'Não conseguimos verificar a sua conexão com a internet. Por favor, tente novamente!',
      buttons: [{
          text: 'Ok',
          handler: data => {
            console.log('Ok clicked');
            this.initializeApp();
          }
        }]
    });
    alert.present();
  }
}
