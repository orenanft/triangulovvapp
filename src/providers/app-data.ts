//Imports nativos localizados em node_modules
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
//import { Observable } from 'rxjs/Observable'; //talvez use mais a frente
import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';

/*
  Generated class for the AppData provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AppData {
  //Variaveis
  private segmentos: any;
  private bares: any;
  private casas: any;
  private alimentacao: any;
  private saude: any;
  private lazer: any;
  private beleza: any;
  private informatica: any;
  private automoveis: any;
  private ads: any;

  constructor(public http: Http) {
    
    console.log('Hello AppData Provider');
    //iniciando segmentos
    this.segmentos = [{id: 0, nome: 'Bares e Casas Noturnas'},
                      {id: 1, nome: 'Casa e Decoração'},
                      {id: 2, nome: 'Alimentação'},
                      {id: 3, nome: 'Saúde'},
                      {id: 4, nome: 'Entretenimento e Lazer'},
                      {id: 5, nome: 'Beleza e Vestuário'},
                      {id: 6, nome: 'Informática'},
                      {id: 7, nome: 'Automóveis'}
                      ];

    //iniciando variaveis json
    this.automoveis = null;
    this.alimentacao = null;
    this.ads = null;
    this.bares = null;
    this.beleza = null;
    this.casas = null;
    this.informatica = null;
    this.lazer = null;
    this.saude = null;
    
  }

  //getters
    getBares(){
      //Variavel com as url(amazon aws s3) do arquivo json
    let baresUrl = 'https://s3-sa-east-1.amazonaws.com/ifup/apps/ilhaHits/json/bares.json';
      if(this.bares){
        return Promise.resolve(this.bares);
      }
      return new Promise(resolve => {
         this.http.get(baresUrl)
                       .map(res => res.json())
                       .subscribe(
                         data => {
                          this.bares = data;
                          resolve(this.bares);
                        },
                         err => {
                          console.log("ERRO HTTP OU ARQUIVO VAZIO");
                          resolve(this.bares);
                        }
                        )
       })
    }

    getCasas(){
      //Variavel com as url(amazon aws s3) do arquivo json
      let casasUrl = 'https://s3-sa-east-1.amazonaws.com/ifup/apps/ilhaHits/json/casa.json';
      if(this.casas){
        return Promise.resolve(this.casas);
      }
      return new Promise(resolve => {
         this.http.get(casasUrl)
                       .map(res => res.json())
                       .subscribe(
                         data => {
                          this.casas = data;
                          resolve(this.casas);
                        },
                         err => {
                          console.log("ERRO HTTP OU ARQUIVO VAZIO");
                          resolve(this.casas);
                        }
                        )
       })
    }
    
    getAlimentacao(){
      //Variavel com as url(amazon aws s3) do arquivo json
      let alimentacaoUrl = 'https://s3-sa-east-1.amazonaws.com/ifup/apps/ilhaHits/json/alimentacao.json';
      if(this.alimentacao){
        return Promise.resolve(this.alimentacao);
      }
      return new Promise(resolve => {
         this.http.get(alimentacaoUrl)
                       .map(res => res.json())
                       .subscribe(
                         data => {
                          this.alimentacao = data;
                          resolve(this.alimentacao);
                        },
                         err => {
                          console.log("ERRO HTTP OU ARQUIVO VAZIO");
                          resolve(this.alimentacao);
                        }
                        )
       })
    }

    getSaude(){
      //Variavel com as url(amazon aws s3) do arquivo json
      let saudeUrl = 'https://s3-sa-east-1.amazonaws.com/ifup/apps/ilhaHits/json/saude.json';
      if(this.saude){
        return Promise.resolve(this.saude);
      }
      return new Promise(resolve => {
         this.http.get(saudeUrl)
                       .map(res => res.json())
                       .subscribe(
                         data => {
                          this.saude = data;
                          resolve(this.saude);
                        },
                         err => {
                          console.log("ERRO HTTP OU ARQUIVO VAZIO");
                          resolve(this.saude);
                        }
                        )
       })      
    }

    getLazer(){
      //Variavel com as url(amazon aws s3) do arquivo json
      let lazerUrl = 'https://s3-sa-east-1.amazonaws.com/ifup/apps/ilhaHits/json/lazer.json';
      if(this.lazer){
        return Promise.resolve(this.lazer);
      }
      return new Promise(resolve => {
         this.http.get(lazerUrl)
                       .map(res => res.json())
                       .subscribe(
                         data => {
                          this.lazer = data;
                          resolve(this.lazer);
                        },
                         err => {
                          console.log("ERRO HTTP OU ARQUIVO VAZIO");
                          resolve(this.lazer);
                        }
                        )
       })
    }

    getBeleza(){
      //Variavel com as url(amazon aws s3) do arquivo json
      let belezaUrl = 'https://s3-sa-east-1.amazonaws.com/ifup/apps/ilhaHits/json/beleza.json';
      if(this.beleza){
        return Promise.resolve(this.beleza);
      }
      return new Promise(resolve => {
         this.http.get(belezaUrl)
                       .map(res => res.json())
                       .subscribe(
                         data => {
                          this.beleza = data;
                          resolve(this.beleza);
                        },
                         err => {
                          console.log("ERRO HTTP OU ARQUIVO VAZIO");
                          resolve(this.beleza);
                        }
                        )
       })
    }

    getInformatica(){
      //Variavel com as url(amazon aws s3) do arquivo json
      let informaticaUrl = 'https://s3-sa-east-1.amazonaws.com/ifup/apps/ilhaHits/json/informatica.json';
      if(this.informatica){
        return Promise.resolve(this.informatica);
      }
      return new Promise(resolve => {
         this.http.get(informaticaUrl)
                       .map(res => res.json())
                       .subscribe(
                         data => {
                          this.informatica = data;
                          resolve(this.informatica);
                        },
                         err => {
                          console.log("ERRO HTTP OU ARQUIVO VAZIO");
                          resolve(this.informatica);
                        }
                        )
       })
    }

    getAutomoveis(){
      //Variavel com as url(amazon aws s3) do arquivo json
      let automoveisUrl = 'https://s3-sa-east-1.amazonaws.com/ifup/apps/ilhaHits/json/automoveis.json';
      if(this.automoveis){
        return Promise.resolve(this.automoveis);
      }
      return new Promise(resolve => {
         this.http.get(automoveisUrl)
                       .map(res => res.json())
                       .subscribe(
                         data => {
                          this.automoveis = data;
                          resolve(this.automoveis);
                        },
                         err => {
                          console.log("ERRO HTTP OU ARQUIVO VAZIO");
                          resolve(this.automoveis);
                        }
                        )
       })
    }

    getAds(){
      //Variavel com as url(amazon aws s3) do arquivo json
      let adsUrl = 'https://s3-sa-east-1.amazonaws.com/ifup/apps/ilhaHits/json/ads.json';
      if(this.ads){
        return Promise.resolve(this.ads);
      }
       return new Promise(resolve => {
         this.http.get(adsUrl)
                       .map(res => res.json())
                       .subscribe(
                         data => {
                          this.ads = data;
                          resolve(this.ads);
                        },
                         err => {
                          console.log("ERRO HTTP OU ARQUIVO VAZIO");
                          resolve(this.ads);
                        }
                        )
       })
    }

    getSegmentos(){
      return Promise.resolve(this.segmentos);
    }


}
