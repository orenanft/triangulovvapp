import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

//webview
import {InAppBrowser} from 'ionic-native';

/*
  Generated class for the Tabinfo page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tabinfo',
  templateUrl: 'tabinfo.html'
})
export class TabinfoPage {

  private empresa:any;
  private mapsUrl:string;
  private ios:boolean;
  private md:boolean;


  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform) {

    this.empresa =  navParams.data;

    //url to maps
    if(this.empresa.maps){
    this.mapsUrl = 'http://maps.google.com/?q='+this.empresa.maps.center[0]+','+this.empresa.maps.center[1]+'&z='+this.empresa.maps.zoom;
    }
  }

  ionViewDidEnter(){
        //set platform
        if (this.platform.is('ios')){
          this.ios=true;
        }else if(this.platform.is('android')){
          this.md=true;
        }
  }

  //launch webview site
  launchSite(url) {
      this.platform.ready().then(() => {
          new InAppBrowser(url, "_blank", "location=yes");
      });
  }

  //launch webview maps
  launch() {
      this.platform.ready().then(() => {
          new InAppBrowser(this.mapsUrl, "_blank", "location=yes");
      });
  }
}
