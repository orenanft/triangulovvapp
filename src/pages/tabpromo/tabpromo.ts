import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

//webview
import {InAppBrowser} from 'ionic-native';

/*
  Generated class for the Tabpromo page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tabpromo',
  templateUrl: 'tabpromo.html'
})
export class TabpromoPage {

  private empresa:any;
  private visible:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform:Platform) {

    this.empresa =  navParams.data;
  }

 isVisible(item) {
        return this.visible === item;
  }

  saibaMais(item) {
        if (this.isVisible(item)) {
            this.visible = null;
        } else {
            this.visible = item;
        }
    }
  //launch webview
  launch(url) {
      this.platform.ready().then(() => {
          new InAppBrowser(url, "_blank", "location=yes");
      });
  }
}
