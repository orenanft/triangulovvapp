//Imports nativos localizados em node_modules
import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';

//Import do provider
import {AppData} from '../../providers/app-data';

//Import da Pagina da Empresa
import { EmpresaPage } from '../empresa/empresa';
/*
  Generated class for the Segmento page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-segmento',
  templateUrl: 'segmento.html'
})
export class SegmentoPage {

  private segmento: any;
  private empresasSegmento: any[];
  private empresasSegmentoBkp: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams, public appData: AppData, public platform: Platform) {
    //get function receive element from HomePage navParams 
    this.segmento= navParams.get('segmento');
    this.empresasSegmento= navParams.get('empresasSegmento');
    //Backup to reset data on searchbar
    this.empresasSegmentoBkp = this.empresasSegmento;

        this.platform.ready().then(() => {
      document.addEventListener('backbutton', () => {
          console.log('Back button tapped');
      }, false);
});
  }
  //Reset data on search
  initializeEmpresasSegmento(){
    this.empresasSegmento = this.empresasSegmentoBkp;
  }

  pushPage(empresa){
    this.navCtrl.push(EmpresaPage, {
      empresaSelected: empresa 
    });
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeEmpresasSegmento();
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.empresasSegmento = this.empresasSegmento.filter((item) => {
        return (item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
