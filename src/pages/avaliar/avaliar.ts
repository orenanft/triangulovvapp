import { Component } from '@angular/core';
import { NavController, NavParams, Platform, MenuController } from 'ionic-angular';

//import InAppBrowser
import {InAppBrowser} from 'ionic-native';
/*
  Generated class for the Avaliar page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-avaliar',
  templateUrl: 'avaliar.html'
})
export class AvaliarPage {

  private title:any;
  private ios:boolean = false;
  private md:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public plt: Platform, public menuCtrl: MenuController) {
    this.title= navParams.get('title');
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad AvaliarPage');
    //disable sidemenu
    this.menuCtrl.enable(false);
    //set platform
        if (this.plt.is('ios')){
          this.ios=true;
        }else if(this.plt.is('android')){
          this.md=true;
        }
  }

  ionViewWillLeave(){
    //enable sidemenu
    this.menuCtrl.enable(true);
  }
  //launch webview
  launch(url) {
      this.plt.ready().then(() => {
          new InAppBrowser(url, "_blank", "location=yes");
      });
  }

}
