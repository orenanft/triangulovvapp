import { Component} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

//google for maps
declare var google;

/*
  Generated class for the Tablocalizacao page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tablocalizacao',
  templateUrl: 'tablocalizacao.html'
})
export class TablocalizacaoPage {

  private empresa:any;
  private map: any;
  private mapElement: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

      this.empresa =  navParams.data;

  }

  // Load map only after view is initialize
  ionViewDidLoad() {
    this.mapElement = document.getElementById('gmaps');
    if(this.mapElement){
      this.loadMap();
    }
  }

    loadMap() {
    // make sure to create following structure in your view.html file
    // and add a height (for example 100%) to it, else the map won't be visible
    // <ion-content>
    //  <div #map id="map" style="height:100%;"></div>
    // </ion-content>

    let latLng = new google.maps.LatLng(this.empresa.maps.center[0], this.empresa.maps.center[1]);
 
    let mapOptions = {
      center: latLng,
      zoom: this.empresa.maps.zoom,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
 
    this.map = new google.maps.Map(this.mapElement, mapOptions);
  
  }

}
