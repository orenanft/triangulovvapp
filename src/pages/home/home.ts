//Imports nativos localizados em node_modules
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, ToastController, MenuController } from 'ionic-angular';
/*slides 
Para documentação completa, veja https://ionicframework.com/docs/v2/api/components/slides/Slides/
*/

//Import do provider
import {AppData} from '../../providers/app-data';

//Import SegmentoPage
import { SegmentoPage } from '../segmento/segmento';

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private slides: any[];
  private haveData:boolean = false;
  @ViewChild(Slides) slideElement: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams, public appData: AppData,private toastCtrl: ToastController, public menuCtrl: MenuController) {
    this.slides=null;
  }

  /*Runs when the page has loaded. This event only happens once per page being created.
  If a page leaves but is cached, then this event will not fire again on a subsequent viewing. 
  The ionViewDidLoad event is good place to put your setup code for the page. */
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
    ionViewWillEnter() {
    console.log('ionViewWillEnter HomePage');

  }
    ionViewDidEnter() {
      console.log('ionViewDidEnter HomePage');
      //enable sidemenu
      this.menuCtrl.enable(true);
      //set slide autoplay
      this.slideMngmt();
    }
    ionViewWillLeave() {
    console.log('ionViewWillLeave HomePage');

  }
    ionViewDidLeave() {
    console.log('ionViewDidLeave HomePage');

  }
    ionViewWillUnload() {
    console.log('ionViewWillUnload HomePage');

  }
    ionViewCanEnter() {
    console.log('ionViewCanEnter HomePage');

  }
    ionViewCanLeave() {
    console.log('ionViewCanLeave HomePage');
    //disale sidemenu
    this.menuCtrl.enable(false);

  }
  //Controle dos Slides
  slideMngmt(){
    //console.log(this.slideElement);
      this.appData.getAds().then(data => {
        if(data.ads.length >= 1) {
          this.haveData=true;
          this.slides = data.ads;
          //not working but should be
          /*setTimeout(()=>{
            if(this.slideElement){
              this.slideElement.initialSlide = '0';
              this.slideElement.autoplay = '2000';
              this.slideElement.loop = true;
              this.slideElement.effect = 'fade';
              this.slideElement.startAutoplay();
              this.slideElement.lockSwipes(true);
            }
          },500);*/
        }
      });
  }

  //Show toast on empresa == null
  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Ainda não há estabelecimentos cadastrados!',
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  pushPage(event){
    // push another page on to the navigation stack
    // causing the nav controller to transition to the new page
    // optional data can also be passed to the pushed page.
    let id = event.currentTarget.getAttribute("id");
    Promise.all([this.loadSegmentos(id), this.loadEmpresaSegmento(id)]).then(data => {
      //Wait for both loadSegmentos and loadEmpresaSegmento return
      //Then use data[0] (loadSegmentos return), data[1] (loadEmpresaSegmento return)
      if(data[1].length == 0){
        this.presentToast();
      }
      else{
        this.navCtrl.push(SegmentoPage, {
        segmento: data[0],
        empresasSegmento: data[1]
      });
      }
    })
    .catch(error => {
      console.log("catch error");
      this.presentToast();
    });
  }

    loadSegmentos(id){
      if ( id === "bares-casas-noturnas") {
        return this.appData.getSegmentos().then(data => {
            return data[0];
        })
      } else if (id === "casa") {
        return this.appData.getSegmentos().then(data => {
            return data[1];
        })
      } else if (id === "alimentacao") {
        return this.appData.getSegmentos().then(data => {
            return data[2];
        })
      } else if (id === "saude") {
        return this.appData.getSegmentos().then(data => {
            return data[3];
        })
      } else if (id === "entretenimento-lazer") {
        return this.appData.getSegmentos().then(data => {
            return data[4];
        })
      } else if (id === "beleza-vestuario") {
        return this.appData.getSegmentos().then(data => {
            return data[5];
        })
      } else if (id === "informatica") {
        return this.appData.getSegmentos().then(data => {
            return data[6];
        })
      } else if (id === "automoveis") {
        return this.appData.getSegmentos().then(data => {
            return data[7];
        }) 
      }
    }

    loadEmpresaSegmento(id){
      
      if ( id === "bares-casas-noturnas") {
        return this.appData.getBares().then(data => {
            return data.bares;
        })
      } else if (id === "casa") {
        return this.appData.getCasas().then(data => {
            return data.casa;
        })
      } else if (id === "alimentacao") {
        return this.appData.getAlimentacao().then(data => {
            return data.alimentacao;
        })
      } else if (id === "saude") {
        return this.appData.getSaude().then(data => {
            return data.saude;
        })
      } else if (id === "entretenimento-lazer") {
        return this.appData.getLazer().then(data => {
            return data.lazer;
        })
      } else if (id === "beleza-vestuario") {
        return this.appData.getBeleza().then(data => {
            return data.beleza;
        })
      } else if (id === "informatica") {
        return this.appData.getInformatica().then(data => {
            return data.informatica;
        })
      } else if (id === "automoveis") {
        return this.appData.getAutomoveis().then(data => {
            return data.automoveis;
        })
      }
    } 
  }
