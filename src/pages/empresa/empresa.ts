import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams, Slides } from 'ionic-angular';

//Import das tabs
import { TabinfoPage } from '../tabinfo/tabinfo';
import { TabpromoPage } from '../tabpromo/tabpromo';
//import { TablocalizacaoPage } from '../tablocalizacao/tablocalizacao';

/*
  Generated class for the Empresa page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-empresa',
  templateUrl: 'empresa.html'
})
export class EmpresaPage {

  @ViewChild(Slides) slider: Slides;

  private empresa: any;
  private tabinfo: any;
  private tabpromo: any;
  //private tablocalizacao:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //get function receive element from HomePage navParams 
    this.empresa = navParams.get('empresaSelected');

    //tabs
    this.tabinfo = TabinfoPage;
    this.tabpromo = TabpromoPage;
    //this.tablocalizacao = TablocalizacaoPage;
  }

}
