import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';

/*
  Generated class for the Contato page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-contato',
  templateUrl: 'contato.html'
})
export class ContatoPage {

  private title:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {
    this.title= navParams.get('title');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SegmentoPage');
    //disable sidemenu
    this.menuCtrl.enable(false);

  }

  ionViewWillLeave(){
    //enable sidemenu
    this.menuCtrl.enable(true);
  }

}
