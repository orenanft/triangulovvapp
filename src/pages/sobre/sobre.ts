import { Component } from '@angular/core';
import { NavController, NavParams, Platform, MenuController } from 'ionic-angular';

//import InAppBrowser
import { InAppBrowser } from 'ionic-native';
/*
  Generated class for the Sobre page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-sobre',
  templateUrl: 'sobre.html'
})
export class SobrePage {

  private title:any;
  private url:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public menuCtrl: MenuController) {
    this.title= navParams.get('title');
    //Url do site
    this.url='';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SegmentoPage');
    //disable sidemenu
    this.menuCtrl.enable(false);

  }

  ionViewWillLeave(){
    //enable sidemenu
    this.menuCtrl.enable(true);
  }

  //launch webview
  launch() {
      this.platform.ready().then(() => {
          new InAppBrowser(this.url, "_blank", "location=yes");
      });
  }
}
